FROM python:3.7.2-alpine3.9

RUN apk add --no-cache build-base jpeg-dev zlib-dev postgresql-dev

RUN adduser --disabled-password --gecos '' --home /waldo waldo

USER waldo
ENV PATH=/waldo/.local/bin:${PATH}
RUN pip install --user pipenv

RUN mkdir /waldo/app /waldo/thumbs
WORKDIR /waldo/app

COPY --chown=waldo:waldo Pipfile Pipfile
COPY --chown=waldo:waldo Pipfile.lock Pipfile.lock

RUN pipenv install --deploy

# remove requirements now they are installed
RUN rm Pipfile Pipfile.lock

COPY --chown=waldo:waldo scripts /waldo/scripts
COPY --chown=waldo:waldo src/services /waldo/app

CMD /waldo/scripts/start.sh
