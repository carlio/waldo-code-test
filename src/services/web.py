import logging
import os
import time
from uuid import UUID

import psycopg2
import requests
from celery import Celery
from flask import Flask, jsonify, request, send_from_directory
from PIL import Image

app = Flask(__name__)

# make the output directory locally for now as there are no credentials to push anywhere

thumbnail_dir = os.environ.get(
    "THUMBNAIL_DIR", os.path.join(os.path.dirname(__file__), "thumbs")
)
if not os.path.exists(thumbnail_dir):
    os.mkdir(thumbnail_dir)


def _get_connection():
    return psycopg2.connect(os.environ["PG_CONNECTION_URI"])


def make_celery(app):
    celery = Celery(app.name, broker=os.environ["AMQP_URI"])
    celery.conf.update(app.config)

    class ContextTask(celery.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery.Task = ContextTask
    return celery


celery = make_celery(app)


@app.route("/")
def index():
    return jsonify(success=True)


@app.route("/photos/pending", methods=["GET"])
def pending_photos():
    cursor = _get_connection().cursor()

    try:
        cursor.execute(
            "SELECT uuid, url, created_at FROM photos WHERE status='pending'"
        )
        results = cursor.fetchall()

        data = []
        for photo in results:
            created_at = time.mktime(photo[2].timetuple())
            data.append({"uuid": photo[0], "url": photo[1], "created_at": created_at})
    finally:
        cursor.close()

    return jsonify(data)


@app.route("/photos/thumbs", methods=["GET"])
def get_thumbnails():
    cursor = _get_connection().cursor()

    try:
        cursor.execute("SELECT uuid, url, photo_uuid FROM photo_thumbnails")
        results = cursor.fetchall()

        data = []
        for thumb in results:
            data.append({"uuid": thumb[0], "url": thumb[1], "photo_uuid": thumb[2]})
    finally:
        cursor.close()

    return jsonify(data)


@app.route("/photos/process", methods=["POST"])
def process_photo():
    data = request.get_json()
    for photo_uuid in data:
        try:
            parsed_uuid = UUID(photo_uuid)
            # TODO: validate photo exists before queuing task or let worker handle it?
            process_photo_task.delay(parsed_uuid)
        except ValueError:
            # bad UUID - just log and carry on
            logging.warning("Bad UUID received in process request: %s", photo_uuid)
    return jsonify(ok=True)


@app.route("/thumbs/<string:filename>")
def send_thumb(filename):
    return send_from_directory(thumbnail_dir, filename)


def create_thumbnail(photo_uuid, url):
    # first get the photo
    r = requests.get(url, allow_redirects=True)
    original_path = os.path.join(thumbnail_dir, "%s-original" % photo_uuid)
    open(original_path, "wb").write(r.content)
    logging.info("Fetched original file to %s", original_path)

    # create a thumbnail of it
    thumb_filename = "%s-thumb.jpg" % photo_uuid
    thumbnail_path = os.path.join(thumbnail_dir, thumb_filename)
    img = Image.open(original_path)
    img.thumbnail((320, 320))
    img.save(thumbnail_path, "JPEG")  # TODO: only handles JPEG at the moment
    logging.info("Thumbnail created at %s", thumbnail_path)

    # remove the copy of the original
    os.remove(original_path)

    # add a record to the database
    conn = _get_connection()
    cursor = conn.cursor()
    try:
        url = "http://localhost:3000/thumbs/%s" % thumb_filename
        cursor.execute(
            "INSERT INTO photo_thumbnails (photo_uuid, width, height, url) VALUES (%s, 320, 320, %s)",
            (photo_uuid, url),
        )
        # note: thumbnail size is fixed at 320x320 for now
        # note2: photos cannot be uploaded anywhere (no creds) so just use the local filename as url
        # finally update the photo status
        cursor.execute(
            "UPDATE photos SET status='completed' WHERE uuid=%s", (photo_uuid,)
        )
        conn.commit()
    finally:
        cursor.close()


@celery.task()
def process_photo_task(photo_uuid: UUID):
    conn = _get_connection()
    cursor = conn.cursor()
    try:
        cursor.execute(
            "SELECT url FROM photos WHERE status='pending' and uuid=%s", (photo_uuid,)
        )
        photo = cursor.fetchone()
        if photo is None:
            logging.warning("Did not find pending photo to process: %s", photo_uuid)
            return
        create_thumbnail(photo_uuid, photo[0])
    except:
        # allow bare except here as the exception is raised
        # if anything failed, update the status
        cursor.execute("UPDATE photos SET status='failed' WHERE uuid=%s", (photo_uuid,))
        conn.commit()
        raise
    finally:
        cursor.close()


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=3000)
