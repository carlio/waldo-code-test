#! /usr/bin/env sh

/waldo/scripts/wait-for.sh waldo-postgres:5432 -- echo "postgres is up"
/waldo/scripts/wait-for.sh waldo-rabbitmq:5672 -- echo "rabbitmq is up"

if [ "$APP_TYPE" == "web" ]; then
  pipenv run python web.py
elif [ "$APP_TYPE" == "worker" ]; then
  pipenv run celery -l info -A web.celery worker
else
  echo "Unrecognised app type $APP_TYPE"
  exit 1
fi
