# Photo Processing

This project implements the deliverables in the original
[project description](./PROJECT-DESCRIPTION.md).

## Running

To try this out locally, `make start_clean` will create all services,
add the required schema and Photo models and start running both the
web server and the message consumer.

http://localhost:3000/photos/pending will list un-processed photos.

To process a photo, `POST` to http://localhost:3000/photos/pending
with a JSON list in the body of UUIDs, ie, `["uuid1", "uuid2"...etc]`

The console output of the processing worker will log what is happening,
there is another endpoint at http://localhost:3000/photos/thumbs to
list the existing thumbnails.

## Changes

### to the Docker setup

I bumped the Python version to 3.7.3 just as it is more recent, this is
a minor change.

I also rearranged it to create and use an unprivileged used to run the
application rather than running as `root`. This is just a paranoid security
issue but I figured I'll add it.

The `docker-compose.yml` now has an extra service (see below) and also
uses volumes in a way in which `docker-down` preserves the data.
`make start` will use existing data while `make start_clean` will delete
those volumes and start fresh. This was just useful while developing
to preserve state and avoid needing new UUIDs in test curl requests etc.

### Message consumer

To build the message consumer I used [celery](http://www.celeryproject.org/)
rather than build a raw AMQP read/write service.

It runs in a separate container when using `docker-compose` since they
are two separate processes.

### Thumbfile location

Since I did not have anywhere to put the thumbs they are simply stored locally
on a volume shared by the web and the worker containers.

There is an endpoint to serve these static files just to be able to see/debug the application at http://localhost:3000/thumbs/<filename> where the filename
is the original photo `UUID-thumb.jpg`.

Note: this only handles JPG.

### Other bits

I ran [black](https://black.readthedocs.io/en/stable/) and [isort](https://readthedocs.org/projects/isort/) to format and tidy the code slightly.
