export PG_USERNAME=waldo
export PG_PASSWORD=1234
export PG_DATABASE=waldo
export PG_CONNECTION_URI=postgres://$(PG_USERNAME):$(PG_PASSWORD)@waldo-postgres/$(PG_DATABASE)

export AMQP_USERNAME=rabbitmq
export AMQP_PASSWORD=1234
export AMQP_VHOST=waldo
export AMQP_URI=amqp://$(AMQP_USERNAME):$(AMQP_PASSWORD)@waldo-rabbitmq:5672/$(AMQP_VHOST)

start: data-up apps-up

start_clean: clean data-up wait-for-data db-schema apps-up

wait-for-data:
	# lazy, should just use scripts/wait-for.sh but this is easy for now
	sleep 10

clean:
	docker-compose down
	docker volume rm -f photo-processor_pgdata photo-processor_rabbitdata photo-processor_thumbs

data-up:
	docker-compose up -d rabbitmq postgres

apps-up:
	docker-compose up --build

db-schema:
	docker exec -i waldo-postgres psql $(PG_CONNECTION_URI) -t < scripts/db-schema.sql

psql:
	docker exec -it waldo-postgres psql $(PG_CONNECTION_URI)
